using ResultLib;

namespace Astral.Interfaces {
    /**
     * Defines behaviours an Astral command should have.
     * 
     * @since 0.0.1
     */
    public interface Command : GLib.Object {
        /**
         * Name of the command to run.
         * 
         * @since 0.0.1
         */
        public abstract string name { get; construct; }

        /**
         * Describes what the command will do  
         * 
         * @since 0.0.1
         */
        public abstract string description { get; construct; }

        /**
         * Execute the command
         * 
         * User will do this by running:
         * {{{
         * astral <command_name>
         * }}}
         * 
         * @since 0.0.1
         */
        public abstract Result<string> run (string[] args);
    }
}
