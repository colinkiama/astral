namespace Astral.Model {
    public class PathSearchArgs : GLib.Object {

        public string directory { get; set; }
        public Gee.List<string> file_types { get; set;}
    }
}
