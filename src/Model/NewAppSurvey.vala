using Astral;
using Astral.Interfaces;
using Survey;
using ResultLib;

namespace Astral.Model {
    /**
     * 
     */
    public class NewAppSurvey : SurveyBase<Gee.HashMap<string, string>> {
      public override Gee.HashMap<string, string> ask (Question[] questions) {
            Gee.HashMap<string, string> survey_answers = new Gee.HashMap<string, string> ();
            foreach (Question<FieldType> question in questions) {
                switch (question.field_type) {
                    case FieldType.NAME:
                        survey_answers["name"] = this.ask_one (question);
                        break;
                    case FieldType.VERSION:
                        survey_answers["version"] = this.ask_one (question);
                        break;
                    case FieldType.DESCRIPTION:
                       survey_answers["description"] = this.ask_one (question);
                        break;
                    case FieldType.AUTHOR:
                        survey_answers["author"] = this.ask_one (question);
                        break;
                    case FieldType.LICENSE:
                        survey_answers["license"] = this.ask_one (question);
                        break;
                    default:
                        break;
                }

            }

            return survey_answers;
        }

    }
}
