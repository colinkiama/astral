namespace Astral.Helpers.CommandHelper {
    public static void list_commands (CommandEntry[] command_list) {
        bool printed_header = false;
        int max_command_name_length = -1;

        foreach (var command in command_list) {
            if (!printed_header) {
                stdout.printf ("Commands:\n");
                printed_header = true;
            }

            if (max_command_name_length < command.name.length) {
                max_command_name_length = command.name.length;
            }
        }

        foreach (var command in command_list) {
            print ("  %s%s     %s\n", command.name,
                string.nfill (max_command_name_length - command.name.length, ' '), command.description
            );
        }
        print ("\n");
    }
}
