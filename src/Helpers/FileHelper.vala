using Astral.Model;
using ResultLib;

namespace Astral.Helpers.FileHelper {
    // TODO: Optimise (concurrency?)
    public static Gee.List<string> recursive_path_search (
        File src,
        File package_root_dir,
        Gee.List<string> file_types,
        Cancellable? cancellable = null) throws Error {

        Gee.List<string> paths_to_add = new Gee.ArrayList<string> ();
        FileType src_type = src.query_file_type (FileQueryInfoFlags.NONE, cancellable);
        if (src_type == FileType.DIRECTORY) {
            string src_path = src.get_path ();
            debug ("Current Directory path: %s\n", src_path);

            FileEnumerator enumerator = src.enumerate_children (
                FileAttribute.STANDARD_NAME,
                FileQueryInfoFlags.NONE,
                cancellable
            );

            for ( FileInfo? info = enumerator.next_file (cancellable); info != null;
                info = enumerator.next_file (cancellable)) {

                paths_to_add.add_all (
                    recursive_path_search (
                        File.new_for_path (Path.build_filename (src_path, info.get_name ())),
                        package_root_dir,
                        file_types,
                        cancellable
                    )
                );
            }
        } else if (src_type == FileType.REGULAR) {
            string src_path = src.get_path ();
            if (file_types.any_match ((file_type) => src_path.contains (file_type))) {
                paths_to_add.add (package_root_dir.get_relative_path (src));
            }
        }

        return paths_to_add;
    }


    // TODO: Optimise (concurrency?)
    public static bool recursive_copy (File src, File dest, FileCopyFlags flags = FileCopyFlags.NONE,
        Cancellable? cancellable = null) throws Error {

        FileType src_type = src.query_file_type (FileQueryInfoFlags.NONE, cancellable);
        if (src_type == FileType.DIRECTORY) {
            if (!dest.query_exists ()) {
                dest.make_directory (cancellable);
            }

            src.copy_attributes (dest, flags, cancellable);

            string src_path = src.get_path ();
            string dest_path = dest.get_path ();

            FileEnumerator enumerator = src.enumerate_children (FileAttribute.STANDARD_NAME,
                FileQueryInfoFlags.NONE, cancellable);

            for ( FileInfo? info = enumerator.next_file (cancellable); info != null;
                info = enumerator.next_file (cancellable)) {

                recursive_copy (
                    File.new_for_path (Path.build_filename (src_path, info.get_name ())),
                    File.new_for_path (Path.build_filename (dest_path, info.get_name ())),
                    flags,
                    cancellable
                );
            }
        } else if (src_type == FileType.REGULAR) {
            src.copy (dest, flags, cancellable);
        }

        return true;
    }
}
