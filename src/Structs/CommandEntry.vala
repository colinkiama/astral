namespace Astral {
    public struct CommandEntry {
        public string name;
        public string description;
    }
}
