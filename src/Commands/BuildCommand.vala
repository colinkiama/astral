using ResultLib;
using Survey;
using Astral.Helpers;

namespace Astral.Commands {
    /**
     * Builds current Astral project
     * 
     */
    public class BuildCommand : Object, Interfaces.Command {
        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public string name { get; construct; }

        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public string description { get; construct; }

        /**
         * Creates new {@link BuildCommand} instance
         * 
         * @since 0.0.1
         */
        public BuildCommand (string name, string description) {
            Object (
                name: name,
                description: description
            );
        }

        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public Result<string> run (string[] args) {
            string package_config_path = Path.build_filename (Environment.get_current_dir (), "vamp.json");
            try {
                string package_config_contents;
                bool did_open = FileUtils.get_contents (package_config_path, out package_config_contents);

                if (!did_open) {
                    error ("Failed to get contents from: %s".printf (package_config_path));
                }

                Vamp.Package package = deserialize_package_config (package_config_contents);
                Gee.ArrayList<string> source_file_paths = new Gee.ArrayList<string> ();
                File package_root_dir = File.new_for_path (Environment.get_current_dir ());

                // TODO: Optimise (concurrency?)
                package.files.foreach ((source_search_query) => {
                    try {
                        source_file_paths.add_all (
                            FileHelper.recursive_path_search (
                                File.new_for_path (Environment.get_current_dir ())
                                    .resolve_relative_path (source_search_query.directory),
                                package_root_dir,
                                source_search_query.file_types
                            )
                        );
                    } catch (Error e) {
                        error (e.message);
                    }

                    return true;
                });

                print ("Source file paths:\n");

                StringBuilder sb = new StringBuilder ("valac -b build");
                sb.append_printf (" -o %s", package.name);
                source_file_paths.foreach ((file_path) => {
                    print ("%s\n", file_path);
                    sb.append_printf (" %s", file_path);
                    return true;
                });

                Pid child_pid;

                try {
                    GLib.Process.spawn_async (
                        null,
                        sb.str.split (" "),
                        Environ.get (),
                        SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
                        null,
                        out child_pid
                    );
                } catch (Error e) {
                    error (e.message);
                }

            } catch (Error e) {
                error (e.message);
            }

            return new Result<string>.with_data ("Build Command Prompts ran!");
        }

        private static Vamp.Package deserialize_package_config (string config_data) {
            var parser = new Json.Parser ();
            try {
                parser.load_from_data (config_data);
                return Vamp.Package.from_json (parser.get_root ());
            } catch (Error e) {
                error ("Unable to parse the package config data: %s\n", e.message);
            }
        }
    }
}
