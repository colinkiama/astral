using ResultLib;
using Survey;

namespace Astral.Commands {
    /**
     * Initialises an Astral package
     * 
     */
    public class InitCommand : Object, Interfaces.Command {
        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public string name { get; construct; }

        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public string description { get; construct; }

        /**
         * Creates new {@link InitCommand} instance
         * 
         * @since 0.0.1
         */
        public InitCommand (string name, string description) {
            Object (
                name: name,
                description: description
            );
        }

        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public Result<string> run (string[] args) {
            var init_survey = new Astral.Model.InitSurvey ();

            print ("Please answer the following questions to create a vamp.json file:\n\n");
            print ("Press ^C at any time to quit.\n");

            Gee.HashMap<string, string> answers = init_survey.ask ({
                Question<FieldType> () {
                    field_type = FieldType.NAME,
                    prompt_message = "Package name",
                    default_answer = "my-package",
                    required = true
                },
                Question<FieldType> () {
                    field_type = FieldType.PACKAGE_TYPE,
                    prompt_message = "Package type [binary/library]",
                    default_answer = "binary",
                    required = true
                },
                Question<FieldType> () {
                    field_type = FieldType.VERSION,
                    default_answer = "0.0.1",
                    prompt_message = "Version",
                    required = true
                },
                Question<FieldType> () {
                    field_type = FieldType.DESCRIPTION,
                    prompt_message = "Description",
                    default_answer = "My new package",
                },
                Question<FieldType> () {
                    field_type = FieldType.AUTHOR,
                    default_answer = "Fake Author <fake@author.fake> (fjsdklfj)",
                    prompt_message = "Author",
                },
                Question<FieldType> () {
                    field_type = FieldType.LICENSE,
                    default_answer = "ISC",
                    prompt_message = "License",
                },
            });

            Vamp.Package package = new Vamp.Package ();
            package.name = answers["name"];
           package.package_type = answers["package_type"];
           package.version = answers["version"];
           package.description = answers["description"];
           package.license = answers["license"];

            Vamp.Person parsed_author;

            if (Vamp.Person.try_parse (answers["author"], out parsed_author)) {
                package.author = parsed_author;
            }

            var generator = new Json.Generator ();
            generator.pretty = true;
            generator.indent = 4;
            generator.set_root (package.to_json ());

            string package_json = generator.to_data (null);

            print ("Generated package configuration data:\n%s\n", package_json);

            File file = File.new_build_filename (Environment.get_current_dir (), "vamp.json");

            try {
                var output_stream = file.create (GLib.FileCreateFlags.NONE);
                output_stream.write (package_json.data);

            } catch (Error error) {

            }

            return new Result<string>.with_data ("Init Command Prompts ran!");
        }
    }
}
