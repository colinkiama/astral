// using Astral;
using ResultLib;
using Survey;
using Astral.Helpers;

namespace Astral.Commands {
    /**
     * Creates new Astral project from template
     * 
     */
    public class NewCommand : Object, Interfaces.Command {
        public static CommandEntry[] command_list = {
            CommandEntry () {
                name = "app",
                description = "Basic Vala program template"
            },
        };
        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public string name { get; construct; }

        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public string description { get; construct; }

        /**
         * Creates new {@link NewCommand} instance
         * 
         * @since 0.0.1
         */
        public NewCommand (string name, string description) {
            Object (
                name: name,
                description: description
            );
        }

        /**
         * {@inheritDoc}
         * 
         * @since 0.0.1
         */
        public Result<string> run (string[] args) {
            if (args.length == 1) {
                // TODO: Print available comamnds under "new".
                CommandHelper.list_commands (command_list);

                return new Result<string>.with_data ("");
            }

            // From now on, we can guarantee that there is more than one arguments length is more than 1.

            switch (args[1]) {
                case "app":
                    return this.run_new_app_survey (args[1:]);
                default:
                    print ("Command not recognised. Try one of these below:\n");
                    CommandHelper.list_commands (command_list);
                    break;
            }

            return new Result<string>.with_data ("");
        }

        private Result<string> run_new_app_survey (string[] args) {
            print ("Please answer the following questions to set up your new Vala app:\n\n");
            print ("Press ^C at any time to quit.\n");

            var survey = new Astral.Model.NewAppSurvey ();

            Gee.HashMap<string, string> answers = survey.ask ({
                Question<FieldType> () {
                    field_type = FieldType.NAME,
                    prompt_message = "Package name",
                    default_answer = "my-package",
                    required = true
                },
                Question<FieldType> () {
                    field_type = FieldType.VERSION,
                    default_answer = "0.0.1",
                    prompt_message = "Version",
                    required = true
                },
                Question<FieldType> () {
                    field_type = FieldType.DESCRIPTION,
                    prompt_message = "Description",
                    default_answer = "My new package",
                },
                Question<FieldType> () {
                    field_type = FieldType.AUTHOR,
                    default_answer = "Fake Author <fake@author.fake> (fjsdklfj)",
                    prompt_message = "Author",
                },
                Question<FieldType> () {
                    field_type = FieldType.LICENSE,
                    default_answer = "ISC",
                    prompt_message = "License",
                },
            });


            Vamp.Package package = new Vamp.Package ();
            package.name = answers["name"];
            package.package_type = "binary";
            package.version = answers["version"];
            package.description = answers["description"];
            package.license = answers["license"];

            package.files = new Gee.ArrayList<Vamp.SourceSearchQuery>.wrap ({
                new Vamp.SourceSearchQuery () {
                    directory = "src/vala",
                    file_types = new Gee.ArrayList<string>.wrap ({
                        ".vala"
                    })
                },
                new Vamp.SourceSearchQuery () {
                    directory = "src/c",
                    file_types = new Gee.ArrayList<string>.wrap ({
                        ".c",
                        ".h"
                    })
                },
                new Vamp.SourceSearchQuery () {
                    directory = "src/vapi",
                    file_types = new Gee.ArrayList<string>.wrap ({
                        ".vapi"
                    })
                },
            });

            Vamp.Person parsed_author;

            if (Vamp.Person.try_parse (answers["author"], out parsed_author)) {
                package.author = parsed_author;
            }

            var generator = new Json.Generator ();
            generator.pretty = true;
            generator.indent = 4;
            generator.set_root (package.to_json ());

            string package_json = generator.to_data (null);

            print ("Generated package configuration data:\n%s\n", package_json);

            string template_path = Path.build_filename (Config.TEMPLATES_DIRECTORY, "app");
            string destination_path = Path.build_filename (Environment.get_current_dir (), package.name);

            File package_config_file = File.new_build_filename (destination_path, "vamp.json");

            try {
                Helpers.FileHelper.recursive_copy (
                    File.new_for_path (template_path),
                    File.new_for_path (destination_path),
                    GLib.FileCopyFlags.OVERWRITE, null
                );

                var output_stream = package_config_file.create (FileCreateFlags.NONE);
                output_stream.write (package_json.data);

            } catch (Error e) {
                error (e.message);
            }

            initialise_git (destination_path);
            add_readme (destination_path, package);
            return new Result<string>.with_data ("New command Prompts ran!");
        }
    }

    private void add_readme (string destination_path, Vamp.Package package) {
        string readme_path = Path.build_filename (destination_path, "README.md");
        try {
            string readme_old_contents;
            FileUtils.get_contents (readme_path, out readme_old_contents);
            string readme_new_contents = readme_old_contents.replace ("{{NAME}}", package.name);
            readme_new_contents = readme_new_contents.replace ("{{DESCRIPTION}}", package.description);
            FileUtils.set_contents (readme_path, readme_new_contents);
        } catch (Error e) {
            warning ("%s\n", e.message);
        }
    }

    private void initialise_git (string destination_path) {
        if (Environment.find_program_in_path ("git") != null) {
            try {
                Process.spawn_sync (
                    destination_path,
                    {"git", "init"},
                    Environ.get (),
                    SpawnFlags.SEARCH_PATH | SpawnFlags.SEARCH_PATH_FROM_ENVP,
                    null
                );

                File destination_dir = File.new_for_path (destination_path);
                destination_dir.get_child (".gitignore").create (FileCreateFlags.NONE).write_all (
                    "build/".data, null
                );
            } catch (Error e) {
                warning ("could not initialize git in the created package directory:\n%s", e.message);
            }
        }

    }
}
