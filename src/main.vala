using Astral.Commands;
using Astral.Interfaces;
using Astral.Model;
using Astral.Helpers;
using ResultLib;

namespace Astral {
    [CCode (array_length = false, array_null_terminated = true)]
    private string[] command_arguments;

    private static bool version = false;
    private const GLib.OptionEntry[] MAIN_ENTRIES = {
        { "version", '\0', OptionFlags.NONE, OptionArg.NONE, ref version, "Display version number", null },
        { OPTION_REMAINING, 0, 0, OptionArg.STRING_ARRAY, ref command_arguments, (string)0, "COMMAND" },
        // list terminator
        { }
    };

    public static int main (string[] args) {
        CommandEntry[] command_list = {
            CommandEntry () {
                name = "init",
                description = "Initialises new Astral project in current directory"
            },
            CommandEntry () {
                name = "new",
                description = "Creates Astral project from template"
            },
            CommandEntry () {
                name = "build",
                description = "Build current Astral project"
            }
        };

        // For more info, check out: https://valadoc.org/glib-2.0/GLib.OptionContext.html
        var opt_context = new OptionContext ("- %s".printf (Config.PROGRAM_SUMMARY));
        try {
            opt_context.set_help_enabled (true);
            opt_context.add_main_entries (MAIN_ENTRIES, null);
            opt_context.parse (ref args);
        } catch (OptionError e) {
            stderr.printf ("error: %s\n", e.message);
            stderr.printf ("Run '%s --help' to see a full list of available command line options.\n", args[0]);
            return 1;
        }

        if (version) {
            stdout.printf ("%s\n", Config.PROGRAM_VERSION);
            return 0;
        }

        if (command_arguments.length == 0) {
            CommandHelper.list_commands (command_list);
            return 0;
        }

        string parsed_command_name = command_arguments[0];

        int command_entry_index;
        if (is_in_command_list (parsed_command_name, command_list, out command_entry_index)) {
            Result<string> command_result;
            switch (parsed_command_name) {
                case "init":
                    CommandEntry current_command = command_list[command_entry_index];
                    command_result = new InitCommand (
                        current_command.name,
                        current_command.description
                    ).run (command_arguments);

                    break;
                case "new":
                    CommandEntry current_command = command_list[command_entry_index];
                    command_result = new NewCommand (
                        current_command.name,
                        current_command.description
                    ).run (command_arguments);

                    break;
                case "build":
                    CommandEntry current_command = command_list[command_entry_index];
                        command_result = new BuildCommand (
                            current_command.name,
                            current_command.description
                        ).run (command_arguments);
                    break;
                default:
                    command_result = new Result<string>.with_data (
                        "'%s' is not handled yet.\n".printf (parsed_command_name)
                    );

                    break;
            }

            if (command_result.has_error ()) {
                error (command_result.expose_error ().message);
            }

            print ("%s\n", command_result.reveal ());
            return 0;
        } else {
            stderr.printf ("`%s` is not a valid command. Run `%s` for a list of commands\n",
                parsed_command_name,
                Config.PROGRAM_NAME
            );
        }

        return 0;
    }

    /**
     * Check if command name is in command list.
     * 
     * outputs index of -1 by default to prevent program. Make sure you are checking the boolean return value
     **/
    private static bool is_in_command_list (string command_name_to_check, CommandEntry[] command_list, out int index) {
        index = -1;

        for (int i = command_list.length - 1; i > -1; i--) {
            var command = command_list[i];

            if (command_name_to_check == command.name ) {
                index = i;
                return true;
            }
        }

        return false;
    }
}
