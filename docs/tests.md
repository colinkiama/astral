# Tests

## Plan on how to implement `tests` field

```json
"tests": [
    {
        "name" : "Core Functions Tests",
        "files": [
            {
                "directory" : "tests/core_funcs",
                "file_types" : [".vala"]
            },
        ]
    }
]
```

Tests may make the package configuration file very big so maybe it will be worth making a vamp-tests.json file that lists all the tests data and allow the user to optionally just refer to that file like this:

```json
"tests": "vamp-tests.json"
```
