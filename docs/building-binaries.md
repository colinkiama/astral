# Building Binaries

When the `package_type` is `app`, this is how the `build` commmand will operate under the hood.

- All source file paths from `files` are gathered into one list.
- Pass all the source files to the compiler.

Maybe all these lists could be stored in on map?
