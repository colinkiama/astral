# {{NAME}}

{{DESCRIPTION}}

### Build

```sh
astral build
```

The output will be in the `build` directory.

You can run the generated binary from there or run:

```sh
astral run
```
