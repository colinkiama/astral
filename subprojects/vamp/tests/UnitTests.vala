namespace Vamp {
    class UnitTests {
        public static int main (string[] args) {
            Test.init (ref args);

            Test.add_func ("/vamp/deserialize_basic_config", () => {
                string package_contents;
                try {
                    bool did_open = FileUtils.get_contents (TestConfig.TEST_PACKAGE_FILE, out package_contents);
                    if (!did_open) {
                        error ("Failed to get contents from: %s".printf (TestConfig.TEST_PACKAGE_FILE));
                    }

                    Vamp.Package package = desrialise_package_config (package_contents);
                    assert_cmpstr (package.name, GLib.CompareOperator.EQ, "test-project");
                    assert_cmpstr (package.version, GLib.CompareOperator.EQ, "0.0.1");
                    assert_cmpstr (package.description, GLib.CompareOperator.EQ, "A Test Project");
                } catch (FileError e) {
                    error (e.message);
                }
            });

            Test.add_func ("/vamp/deserialize_full_config", () => {
                string package_contents;
                try {
                    bool did_open = FileUtils.get_contents (TestConfig.FULL_TEST_PACKAGE_FILE, out package_contents);
                    if (!did_open) {
                        error ("Failed to get contents from: %s".printf (TestConfig.FULL_TEST_PACKAGE_FILE));
                    }

                    Vamp.Package package = desrialise_package_config (package_contents);
                    assert_cmpstr (package.name, GLib.CompareOperator.EQ, "test-project");
                    assert_cmpstr (package.version, GLib.CompareOperator.EQ, "0.0.1");
                    assert_cmpstr (package.description, GLib.CompareOperator.EQ, "A Test Project");
                    assert_cmpstrv (package.keywords.to_array (), {"test", "project", "fake", "mock"});
                    assert_cmpstr (package.homepage, GLib.CompareOperator.EQ, "https://wwww.test.com");

                    // We assert objects this way so that:
                    // 1. We know the exact value of each object property
                    // 2. We can easily update our asserts as we update the
                    // object's properties.
                    assert_full_config_bugs (package.bugs);
                    assert_cmpstr (package.license, GLib.CompareOperator.EQ, "MIT");
                    assert_full_config_author (package.author);
                    assert_full_config_contributors (package.contributors);
                    assert_full_config_funding (package.funding);
                    assert_full_config_files (package.files);
                    assert_full_config_respository (package.repository);
                    assert_full_config_dependencies (package.dependencies);
                    assert_full_config_dev_dependencies (package.dev_dependencies);
                    assert_full_config_optional_dependencies (package.optional_dependencies);
                } catch (FileError e) {
                    error (e.message);
                }
            });

            Test.add_func ("/vamp/serialize_basic_config", () => {
                Vamp.Package package = new Vamp.Package ();
                package.name = "basic-project";
                package.version = "1.0.0";
                package.description = "A basic project";

                var generator = new Json.Generator ();
                generator.pretty = true;
                generator.indent = 4;
                generator.set_root (package.to_json ());
                try {
                    string expected_content;
                    bool did_open = FileUtils.get_contents (TestConfig.BASIC_EXPECTED_TEST_PACKAGE_FILE,
                        out expected_content
                    );

                    if (!did_open) {
                        error ("Failed to get contents from: %s".printf (TestConfig.BASIC_EXPECTED_TEST_PACKAGE_FILE));
                    }

                    assert_cmpstr (generator.to_data (null), CompareOperator.EQ, expected_content);

                } catch (Error e) {
                    error (e.message);
                }
            });

            Test.add_func ("/vamp/serialize_full_config", () => {
                Vamp.Package package = new Vamp.Package ();
                package.name = "full-project";
                package.version = "1.0.0";
                package.description = "A full project";
                package.keywords = new Gee.ArrayList<string>.wrap ({"full", "project"});
                package.homepage = "https://www.full-project.com";
                package.bugs = new Bugs () {
                    url = "https://www.full-project.com/bugs",
                    email = "bugs@full-project.com"
                };

                package.license = "MIT";
                package.author = new Person () {
                    name = "vamp-dev",
                    email = "vamp-dev@vamp.org",
                    url = "https://www.vamp-dev.com"
                };

                package.contributors = new Gee.ArrayList<Person>.wrap ({
                    new Person () {
                        name = "vamp-dev-2",
                        email = "vamp-dev-2@vamp.org",
                        url = "https://www.vamp-dev-2.com"
                    },
                    new Person () {
                        name = "vamp-dev-3",
                        email = "vamp-dev-3@vamp.org",
                        url = "https://www.vamp-dev-3.com"
                    },
                });

                package.funding = new Gee.ArrayList<FundingInfo>.wrap ({
                    new FundingInfo () {
                        funding_type = "individual",
                        url = "https://www.vamp.com/donate"
                    }
                });

                package.files = new Gee.ArrayList<Vamp.SourceSearchQuery>.wrap ({
                    new SourceSearchQuery () {
                        directory = "src/vala",
                        file_types = new Gee.ArrayList<string>.wrap ({ ".vala" })
                    },
                    new SourceSearchQuery () {
                        directory = "src/c",
                        file_types = new Gee.ArrayList<string>.wrap ({ ".c", ".h" })
                    },
                    new SourceSearchQuery () {
                        directory = "src/vapi",
                        file_types = new Gee.ArrayList<string>.wrap ({ ".vapi" })
                    },
                });

                package.repository = new Repository () {
                    repository_type = "git",
                    url = "https://www.notgithub.com/owner/project"
                };

                package.dependencies = new Gee.ArrayList<Vamp.Dependency>.wrap ({
                    new Vamp.Dependency () {
                        name = "json-glib",
                        version = "^1.0.0",
                        url = "https://wiki.gnome.org/Projects/JsonGlib",
                    }
                });

                package.dev_dependencies = new Gee.ArrayList<Vamp.Dependency>.wrap ({
                    new Vamp.Dependency () {
                        name = "g-ir-compiler",
                        version = "^1.2.0",
                        url = "https://gitlab.gnome.org/GNOME/gobject-introspection"
                    }
                });

                package.optional_dependencies = new Gee.ArrayList<Vamp.Dependency>.wrap ({
                    new Vamp.Dependency () {
                        name = "libvaladoc",
                        version = "^0.56.0",
                        url = "https://wiki.gnome.org/Projects/Valadoc",
                        legacy = true
                    }
                });

                var generator = new Json.Generator ();
                generator.pretty = true;
                generator.indent = 4;
                generator.set_root (package.to_json ());
                print ("Generated Package config:\n%s\n", generator.to_data (null));

                try {
                    string expected_content;
                    bool did_open = FileUtils.get_contents (TestConfig.FULL_EXPECTED_TEST_PACKAGE_FILE,
                        out expected_content
                    );

                    if (!did_open) {
                        error ("Failed to get contents from: %s".printf (TestConfig.FULL_EXPECTED_TEST_PACKAGE_FILE));
                    }

                    assert_cmpstr (generator.to_data (null), CompareOperator.EQ, expected_content);

                } catch (Error e) {
                    error (e.message);
                }
            });

            return Test.run ();
        }

        private static void assert_full_config_files (Gee.List<SourceSearchQuery> queries) {
            var query = queries[0];
            assert_cmpstr (query.directory, GLib.CompareOperator.EQ, "src/vala");
            assert_cmpstr (query.file_types[0], GLib.CompareOperator.EQ, ".vala");
        }

        private static void assert_full_config_optional_dependencies (Gee.List<Vamp.Dependency> dependencies) {
            var dep = dependencies[0];
            assert_cmpstr (dep.name, GLib.CompareOperator.EQ, "libvaladoc");
            assert_cmpstr (dep.version, GLib.CompareOperator.EQ, "^0.56.0");
            assert_cmpstr (dep.url, GLib.CompareOperator.EQ, "https://wiki.gnome.org/Projects/Valadoc");
            assert (dep.legacy == true);
        }

        private static void assert_full_config_dev_dependencies (Gee.List<Vamp.Dependency> dependencies) {
            var dep = dependencies[0];
            assert_cmpstr (dep.name, GLib.CompareOperator.EQ, "g-ir-compiler");
            assert_cmpstr (dep.version, GLib.CompareOperator.EQ, "^1.2.0");
            assert_cmpstr (
                dep.url,
                GLib.CompareOperator.EQ,
                "https://gitlab.gnome.org/GNOME/gobject-introspection"
            );
        }

        private static void assert_full_config_dependencies (Gee.List<Vamp.Dependency> dependencies) {
            var dep = dependencies[0];
            assert_cmpstr (dep.name, GLib.CompareOperator.EQ, "json-glib");
            assert_cmpstr (dep.version, GLib.CompareOperator.EQ, "^1.6.0");
            assert_cmpstr (dep.url, GLib.CompareOperator.EQ, "https://wiki.gnome.org/Projects/JsonGlib");
        }

        private static void assert_full_config_respository (Vamp.Repository repository) {
            assert_cmpstr (repository.repository_type, GLib.CompareOperator.EQ, "git");
            assert_cmpstr (repository.url, GLib.CompareOperator.EQ, "https://www.notgithub.com/owner/project");
        }

        private static void assert_full_config_funding (Gee.List<FundingInfo> funding) {
            for (int i = 0; i < funding.size; i++) {
                FundingInfo funding_info = funding[i];
                switch (i) {
                    case 0:
                        assert_cmpstr (funding_info.funding_type, CompareOperator.EQ, "individual");
                        assert_cmpstr (funding_info.url, CompareOperator.EQ, "https://www.vamp.com/donate");
                        break;
                }

                if (i == funding.size - 1 && i != 0) {
                    Test.message ("Test failed! - Did not parse 1 funding info item.\n"
                        + "Parsed: %d parsing info item(s).", i + 1
                    );

                    Test.fail ();
                }
            }
        }
        private static void assert_full_config_contributors (Gee.List<Person> contributors) {
            for (int i = 0; i < contributors.size; i++) {
                Person contributor = contributors[i];
                switch (i) {
                    case 0:
                        assert_cmpstr (contributor.name, CompareOperator.EQ, "vamp-dev-2");
                        assert_cmpstr (contributor.email, CompareOperator.EQ, "vamp-dev-2@vamp.org");
                        assert_cmpstr (contributor.url, CompareOperator.EQ, "https://vamp-dev-2.com");
                        break;
                    case 1:
                        assert_cmpstr (contributor.name, CompareOperator.EQ, "vamp-dev-3");
                        assert_cmpstr (contributor.email, CompareOperator.EQ, "vamp-dev-3@vamp.org");
                        assert_cmpstr (contributor.url, CompareOperator.EQ, "https://vamp-dev-3.com");
                        break;
                }

                if (i == contributors.size - 1 && i != 1) {
                    Test.message ("Test failed! - Did not parse 2 contributors.\nParsed: %d contributor(s).", i + 1);
                    Test.fail ();
                }
            }
        }

        private static void assert_full_config_author (Vamp.Person author) {
            assert_cmpstr (author.name, CompareOperator.EQ, "vamp-dev");
            assert_cmpstr (author.email, CompareOperator.EQ, "vamp-dev@vamp.org");
            assert_cmpstr (author.url, CompareOperator.EQ, "https://vamp-dev.com");
        }

        private static void assert_full_config_bugs (Vamp.Bugs bugs) {
            assert_cmpstr (bugs.email, CompareOperator.EQ, "bugs@test.com");
            assert_cmpstr (bugs.url, CompareOperator.EQ, "https://www.notgithub.com/owner/project/issues");
        }

        private static Vamp.Package desrialise_package_config (string config_data) {
            var parser = new Json.Parser ();
            try {
                parser.load_from_data (config_data);
                return Vamp.Package.from_json (parser.get_root ());
            } catch (Error e) {
                error ("Unable to parse the package config data: %s\n", e.message);
            }
        }
    }
}
