<div align="center">
  <h1 align="center"><center>vamp</center></h1>
  <h3 align="center"><center>A package manager for Vala (WIP)</center></h3>
</div>

---

## Dependencies

- valac
- gio-2.0
- glib-2.0
- gobject-2.0
- gee-0.8
- json-glib-1.0
- valadoc (Optional) - For generating documentation.

## Planned features

- Fetch and install Vala dependencies from Git URLs
- Integrate with Meson and Flatpak

## vamp.json spec inspirations

- npm: https://docs.npmjs.com/cli/v8/configuring-npm/package-json
- drakkar: https://github.com/valum-framework/drakkar
