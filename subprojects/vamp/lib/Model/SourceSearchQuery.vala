public class Vamp.SourceSearchQuery : Object {
    public string directory { get; set; }
    public Gee.List<string> file_types { get; set; }

    public static Vamp.SourceSearchQuery from_json (Json.Node node) {
        assert (node.get_node_type () == OBJECT);
        var result = new Vamp.SourceSearchQuery ();

        var obj = node.get_object ();
        obj.get_members ().foreach ((member_name) => {
            switch (member_name) {
                case "file_types":
                    var array = obj.get_array_member (member_name);
                    if (array.get_length () == 0) {
                        break;
                    }

                    var file_types_to_add = new Gee.ArrayList<string> ();

                    array.foreach_element ((_, __, element_node) => {
                        if (element_node.get_node_type () != Json.NodeType.VALUE) {
                            return;
                        }

                        file_types_to_add.add (element_node.get_string ());
                    });

                    result.set_property (member_name, file_types_to_add);
                    break;
                default:
                    result.set_property (member_name, obj.get_string_member (member_name));
                    break;
            }
        });

        return result;
    }

    public Json.Node to_json () {
        var obj = new Json.Object ();


        if (this.directory != null) {
            obj.set_string_member ("directory", this.directory);
        }

        if (this.file_types != null && this.file_types.size > 0) {
            var node_array = new Json.Array.sized (this.file_types.size);

            this.file_types.foreach ((element) => {
                var value_node = new Json.Node (Json.NodeType.VALUE);
                value_node.set_string (element);
                node_array.add_element (value_node);
                return true;
            });

            obj.set_array_member ("file_types", node_array);
        }


        var result = new Json.Node (Json.NodeType.OBJECT);
        result.set_object (obj);

        return result;
    }

    public static Json.Node list_to_json (Gee.List<SourceSearchQuery> list) {
        var node_array = new Json.Array.sized (list.size);

        list.foreach ((element) => {
            node_array.add_element (element.to_json ());
            return true;
        });

        var node = new Json.Node (Json.NodeType.ARRAY);
        node.set_array (node_array);
        return node;
    }

    public static Gee.List<SourceSearchQuery> list_from_json (Json.Node node) {
        assert (node.get_node_type () == ARRAY);

        var array = node.get_array ();
        var result = new Gee.ArrayList<SourceSearchQuery> ();

        array.foreach_element ((_, __, element_node) => {
            if (element_node.get_node_type () != OBJECT) {
                return;
            }

            result.add (SourceSearchQuery.from_json (element_node));
        });

        return result;
    }
}
