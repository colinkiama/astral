/**
 * 
 * 
 */
public class Vamp.Dependency : Object {
    public string name { get; set; }
    public string version { get; set; }
    public string url { get; set; }
    /**
     * This tells the package manager to try find the package in the system through
     * using the older ways of finding Vala packages e.g: (pkg-config etc)
     */
    public bool legacy { get; set; }

    public static Vamp.Dependency from_json (Json.Node node) {
        assert (node.get_node_type () == OBJECT);
        return (Vamp.Dependency) Json.gobject_deserialize (typeof (Vamp.Dependency), node);
    }

    public Json.Node to_json () {
        return Json.gobject_serialize (this);
    }

    public static Gee.List<Vamp.Dependency> list_from_json (Json.Node node) {
        assert (node.get_node_type () == ARRAY);

        var array = node.get_array ();
        var result = new Gee.ArrayList<Vamp.Dependency> ();

        array.foreach_element ((_, __, element_node) => {
            if (element_node.get_node_type () != OBJECT) {
                return;
            }

            result.add (Vamp.Dependency.from_json (element_node));
        });

        return result;
    }

    public static Json.Node list_to_json (Gee.List<Vamp.Dependency> list) {
        var node_array = new Json.Array.sized (list.size);

        list.foreach ((element) => {
            node_array.add_element (element.to_json ());
            return true;
        });

        var node = new Json.Node (Json.NodeType.ARRAY);
        node.set_array (node_array);
        return node;
    }

}
