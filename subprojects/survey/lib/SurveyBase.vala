using ResultLib;

namespace Survey {
    public abstract class SurveyBase<T> : Survey<T>, Object {
        public abstract T ask (Question[] questions);

        // TODO: Handle validation delegate/type enum
        // TODO: Handle "required" field
        // TODO: Handle available_options
        protected virtual string ask_one (Question question) {
            print ("%s: (%s) ", question.prompt_message, question.default_answer);
            string answer = stdin.read_line ().strip ();
            if (answer == "" && question.default_answer == null) {
                return answer;
            } else if (answer == "" && question.default_answer != null) {
                return question.default_answer;
            }

            return answer;
        }
    }
}
