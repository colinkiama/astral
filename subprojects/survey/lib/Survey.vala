using ResultLib;

namespace Survey {
    // Inspired by Go Survey package of the same name: https://github.com/AlecAivazis/survey 
    /**
     * Helper for asking users questions in the terminal
     * 
     */

    public interface Survey<T> : Object {
        public abstract T ask (Question[] questions);
        // TODO: Add validation delegate/type enum field
        protected abstract string ask_one (Question question);
    }
}
