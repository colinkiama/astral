namespace Survey {
    // TODO: Over time, fill add more entries until you cover all the fields.
    public enum FieldType {
        NAME,
        PACKAGE_TYPE,
        VERSION,
        DESCRIPTION,
        AUTHOR,
        LICENSE
    }
}
