namespace Survey {
    // TODO: Add "available_options" field
    // TODO: Create an optional type (Optional<T>) instead of relying on null.
    public struct Question<T> {
        T field_type;
        string prompt_message;
        string default_answer;
        bool required;
    }
}
