namespace ResultLib {
    /**
     * Result of an operation
     * 
     * @since 0.0.1
     */
    public class Result<T> : GLib.Object {
        private T data;
        private Error error;

        /**
         * Construct new succcessful result object
         * 
         * @since 0.0.1
         */
        public Result.with_data (T data) {
            this.data = data;
        }

        /**
         * Construct new failed result object
         * 
         * @since 0.0.1
         */
        public Result.with_error (Error error) {
            this.error = error;
        }

        /**
         * Get data from result
         * 
         * @since 0.0.1
         */
        public T reveal () {
            return data;
        }

         /**
         * Get error details from result
         * 
         * Call {@link has_error} first
         * before calling this command.
         * 
         * @since 0.0.1
         */
        public Error expose_error () {
            return error;
        }

        /**
         * Check if result contains error
         * 
         * @since 0.0.1
         */
        public bool has_error () {
            return error != null;
        }
    }
}
